from filtres.filtreElectricien import FiltreElectricien
from filtres.filtreAvocat import FiltreAvocat

class FactoryFiltres:
	@staticmethod
	def make(type):
		if type == "electricien":
			return FiltreElectricien()
		if type == "avocat":
			return FiltreAvocat()

	@staticmethod
	def getAllProfilInfo():
		return [
			{
				"profil" : "electricien",
				"labelProfil" : "Electricien",
				"filtres": [
					{"id" : "24h", "label" : "24H", "default" : False},
					{"id" : "urgence", "label" : "Urgence", "default" : False},
					{"id" : "express", "label" : "Express", "default" : False},
					{"id" : "renovation", "label" : "Renovation", "default" : False},
					{"id" : "miseAuNorme", "label" : "Mise aux normes", "default" : False}
				]
			},
			{
				"profil" : "avocat",
				"labelProfil" : "Avocat",
				"filtres": [
					{"id" : "affairesCiviles", "label" : "Affaires civile", "default" : False},
					{"id" : "affairesPenales", "label" : "Affaires penales", "default" : False},
					{"id" : "commisOffice", "label" : "Commis d'Office", "default" : False},
					{"id" : "aideJuridictionnelle", "label" : "Aide juridictionnelle", "default" : False},
					{"id" : "generaliste", "label" : "Generaliste", "default" : False},
					{"id" : "droitDeLaFamille", "label" : "Droit de la famille", "default" : False},
					{"id" : "droitImmobilier", "label" : "Droit de l'immobilier", "default" : False},
					{"id" : "permisDeConstruire", "label" : "Permis de construire", "default" : False},
					{"id" : "droitPenal", "label" : "Droit penal", "default" : False},
					{"id" : "droitSocial", "label" : "Droit social", "default" : False},
					{"id" : "droitDuTravail", "label" : "Droit du travail", "default" : False},
					{"id" : "droitDesAffaires", "label" : "Droit des affaires", "default" : False},
					{"id" : "droitFinancier", "label" : "Droit financier", "default" : False},
					{"id" : "droitFinancier", "label" : "Droit financier", "default" : False},
					{"id" : "droitInternational", "label" : "Droit international", "default" : False},
					{"id" : "droitAdministratif", "label" : "Droit administratif", "default" : False},
					{"id" : "enUrgence", "label" : "En urgence", "default" : False},
					{"id" : "droitRoutier", "label" : "Droit routier", "default" : False}
				]
			}
		]