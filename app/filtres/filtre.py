from nltk.stem.snowball import FrenchStemmer

class Filtre:
	def filterByListeWord(self, data, listWord, index = 3):
		stemmer = FrenchStemmer()
		for w in listWord:
			data = [x for x in data if x[3].find(stemmer.stem(w)) == -1]
		return data