from nltk.stem.snowball import FrenchStemmer
from filtres.filtre import Filtre

class FiltreElectricien(Filtre):
	def __init__(self):
		self.filtres = {
			"24h" : False,
			"urgence" : False,
			"express" : False,
			"renovation" : False,
			"miseAuNorme" : False
		}
		self.dataBrut = []


	def init(self, dataBrut, options):
		for k in self.filtres.keys():
			if k in options:
				self.filtres[k] = options[k]
		
		self.dataBrut = dataBrut		


	def run(self):
		self.dataBrut = self.filter24h()
		self.dataBrut = self.filterUrgence()
		self.dataBrut = self.filterExpress()
		self.dataBrut = self.filterRenovation()
		self.dataBrut = self.filterMiseAuNorme()
		return (self.dataBrut)


	def filter24h(self):
		if self.filtres["24h"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"24h",
			"24"
		])


	def filterUrgence(self):
		if self.filtres["urgence"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"urgence",
			"urgent"
		])


	def filterExpress(self):
		if self.filtres["express"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"express"
		])


	def filterRenovation(self):
		if self.filtres["renovation"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"renovation"
		])


	def filterMiseAuNorme(self):
		if self.filtres["miseAuNorme"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"norme",
			"conformité"
		])

