from nltk.stem.snowball import FrenchStemmer
from filtres.filtre import Filtre


class FiltreAvocat(Filtre):
	def __init__(self):
		self.filtres = {
			"affairesCiviles" : False,
			"affairesPenales" : False,
			"commisOffice" : False,
			"aideJuridictionnelle" : False,
			"specialiseDroitSocial" : False,
			"generaliste" : False,
			"droitDeLaFamille" : False,
			"droitImmobilier" : False,
			"permisDeConstruire" : False,
			"droitPenal" : False,
			"droitSocial" : False,
			"droitDuTravail" : False,
			"droitDesAffaires" : False,
			"droitFinancier" : False,
			"droitDesEntreprises" : False,
			"droitInternational" : False,
			"droitAdministratif" : False,
			"droitRoutier" : False,
			"enUrgence" : False,
		}
		self.dataBrut = []


	def init(self, dataBrut, options):
		for k in self.filtres.keys():
			if k in options:
				self.filtres[k] = options[k]

		self.dataBrut = dataBrut		


	def run(self):
		self.dataBrut = self.filterAffairesCiviles()
		self.dataBrut = self.filterAffairesPenales()
		self.dataBrut = self.filterCommisOffice()
		self.dataBrut = self.filterAideJuridictionnelle()
		self.dataBrut = self.filterSpecialiseDroitSocial()
		self.dataBrut = self.filterGeneraliste()
		self.dataBrut = self.filterDroitDeLaFamille()
		self.dataBrut = self.filterDroitImmobilier()
		self.dataBrut = self.filterPermisDeConstruire()
		self.dataBrut = self.filterDroitPenal()
		self.dataBrut = self.filterDroitSocial()
		self.dataBrut = self.filterDroitDuTravail()
		self.dataBrut = self.filterDroitDesAffaires()
		self.dataBrut = self.filterDroitFinancier()
		self.dataBrut = self.filterDroitDesEntreprises()
		self.dataBrut = self.filterDroitInternational()
		self.dataBrut = self.filterDroitAdministratif()
		self.dataBrut = self.filterEnUrgence()
		self.dataBrut = self.filterAffairesRoutieres()
		return (self.dataBrut)



	def filterAffairesRoutieres(self):
		if self.filtres["droitRoutier"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"voiture",
			"routier",
			"route",
			"automobile",
			"vitesse",
			"permis",
			"pemis",
			"contravention",
			"volant"
		])


	def filterAffairesCiviles(self):
		if self.filtres["affairesCiviles"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"civil"
		])


	def filterAffairesPenales(self):
		if self.filtres["affairesPenales"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"penal"
		])


	def filterCommisOffice(self):
		if self.filtres["commisOffice"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"commis", 
			"office", 
			"gratuit"
		])


	def filterAideJuridictionnelle(self):
		if self.filtres["aideJuridictionnelle"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"aide juridictionnelle",
			"l'aide juridictionnelle",
			"juridictionnelle",
			"juridictionelle"
		])


	def filterSpecialiseDroitSocial(self):
		if self.filtres["specialiseDroitSocial"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"social"
		])


	def filterGeneraliste(self):
		if self.filtres["generaliste"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"cher",
			"maitre",
			"cabinet",
			"service avocat",
			"maître avocat",
			"cabinet d'avocat",
			"avocat conseil",
			"pas cher",
			"trouver un avocat",
			"service",
			"spécialisé"
			"trover",
			"justice",
			"spécialisé"
		])


	def filterDroitDeLaFamille(self):
		if self.filtres["droitDeLaFamille"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"separation",
			"pere",
			"garde",
			"famille",
			"familiale",
			"divorce",
			"divorcer"
		])


	def filterDroitImmobilier(self):
		if self.filtres["droitImmobilier"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"immobilier",
			"construction",
			"batiment",
			"urbanisme",
			"loge",
			"matrimonial"
		])


	def filterPermisDeConstruire(self):
		if self.filtres["permisDeConstruire"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"permis",
			"construire",
			"batiment",
			"urbanisme",
			"logement"
		])


	def filterDroitPenal(self):
		if self.filtres["droitPenal"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"penal",
			"penaliste",
			"instance",
			"assise",
			"correctionnel"
		])


	def filterDroitSocial(self):
		if self.filtres["droitSocial"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"social"
		])


	def filterDroitDuTravail(self):
		if self.filtres["droitDuTravail"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"travail",
			"prudhomme",
			"prud hommes",
			"licenciement"
		])


	def filterDroitDesAffaires(self):
		if self.filtres["droitDesAffaires"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"affaires",
			"commercial",
			"commerce",
			"entreprise",
			"économique",
			"financier",
			"economique"
		])


	def filterDroitFinancier(self):
		if self.filtres["droitFinancier"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"financier"
		])


	def filterDroitDesEntreprises(self):
		if self.filtres["droitDesEntreprises"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"entreprise"
		])


	def filterDroitInternational(self):
		if self.filtres["droitInternational"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"international"
		])


	def filterDroitAdministratif(self):
		if self.filtres["droitAdministratif"] == False:
			return self.dataBrut

		print("testssss")
		return self.filterByListeWord(self.dataBrut, [
			"publique",
			"public",
			"administratif",
			"fonctionnaire"
		])


	def filterEnUrgence(self):
		if self.filtres["enUrgence"] == False:
			return self.dataBrut

		return self.filterByListeWord(self.dataBrut, [
			"urgence",
			"urgent",
			"rapidement"
		])

