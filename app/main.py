from flask import Flask, request, render_template
from flask import jsonify
from flask_cors import CORS, cross_origin
from filtres.factoryFiltres import FactoryFiltres 
import traitementCSV
import clusterisation
import json

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'



@app.route("/clustering", methods = ['POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def clusteringFacto():
    options = json.loads(request.form["options"])
    pathSave = traitementCSV.getFileMotCle(options["profil"])
    if pathSave == False:
        return "erreur fichier introuvable"

    dataBrut = traitementCSV.importAndFiltre(pathSave, options)
    data = clusterisation.buildDendrogramJson(dataBrut, options)
    return jsonify(data)



@app.route("/clustering/dendrogram", methods = ['POST'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def dendrogram():
    options = json.loads(request.form["options"])
    pathSave = traitementCSV.getFileMotCle(options["profil"])
    if pathSave == False:
        return "erreur fichier introuvable"

    options = json.loads(request.form["options"])
    dataBrut = traitementCSV.importAndFiltre(pathSave, options)
    imagePath = clusterisation.buildDendrogramImg(dataBrut, pathSave, options)
    return "<img src='" + request.url_root + imagePath + "'>"



@app.route("/clustering/profils", methods = ['GET'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def profils():
    profils = FactoryFiltres.getAllProfilInfo()
    profils = sorted(profils, key=lambda x: x["labelProfil"])
    for i in range(0, len(profils)):
        profils[i]["filtres"] = sorted(profils[i]["filtres"], key=lambda x: x["label"])
    return jsonify(profils)



if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
