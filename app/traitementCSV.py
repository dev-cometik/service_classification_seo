# -*- coding: utf-8 -*

import csv
from nltk.stem.snowball import FrenchStemmer
from nltk import RegexpTokenizer
from stop_words import get_stop_words
from werkzeug import secure_filename
from scipy.cluster.hierarchy import dendrogram, linkage, to_tree, fcluster
from filtres.factoryFiltres import FactoryFiltres 
import numpy as np
import os
import unidecode
import json


class ExceptionFilterNoResult(Exception):
    pass



def cleanText(input):
	input = unidecode.unidecode(input)
	stemmer = FrenchStemmer()
	stopWords = get_stop_words('fr')
	tokenizer = RegexpTokenizer(r'\w+')
	listWords = [x.lower() for x in tokenizer.tokenize(input)]
	listWords = [x for x in listWords if x not in stopWords]
	listWords = [stemmer.stem(x) for x in listWords]
	return ' '.join(listWords)


def supprimerDoublonsInitiaux(input):
	tempo = []
	final = []
	for x in input:
		if x[0] not in tempo:
			tempo.append(x[0])
			final.append(x)

	return final


def supprimerDoublonsLeman(input):
	tempo = []
	final = []
	for x in input:
		if x[3] not in tempo:
			tempo.append(x[3])
			final.append(x)

	return final


def importation(pathFile, options = {}):
	with open(pathFile, newline='', encoding='utf8') as csvfile:
		dataBrut = list(csv.reader(csvfile, delimiter=';'))
		dataBrut = dataBrut[1:] if options["includeFirstLine"] == False else dataBrut
		dataBrut = supprimerDoublonsInitiaux(dataBrut)

	dataBrut = [[x[0], int(x[1]), float(x[2].replace(",", ".")), cleanText(x[0])] for x in dataBrut]
	if options["ignoreDoublonsLeman"]:
		dataBrut = supprimerDoublonsLeman(dataBrut)

	
	return dataBrut 


def filtrage(dataBrut, options):
	if "profil" not in options or "profil_filtres" not in options:
		return dataBrut

	filtre = FactoryFiltres.make(options["profil"])
	filtre.init(dataBrut, options["profil_filtres"])
	dataWithFiltre = filtre.run()

	if len(dataWithFiltre) == 0:
		raise ExceptionFilterNoResult("Aucune donnée après le filtrage")
	
	return dataWithFiltre


def importAndFiltre(pathFile, options):
	dataBrut = importation(pathFile, options)
	return filtrage(dataBrut, options)


def saveFile(req):
	if 'file' not in req.files:
		return False

	f = req.files['file']
	pathSave = "upload/" + secure_filename(f.filename)
	f.save(pathSave)
	return pathSave


def getFileMotCle(profil):
	path = "upload/" + profil + ".csv"
	if os.path.exists(path) == False:
		return False

	return path


