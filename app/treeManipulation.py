from functools import reduce

def add_node(node, parent ):
	newNode = dict( node_id=node.id, children=[] )
	parent["children"].append( newNode )

	if node.left:
		add_node( node.left, newNode )
	if node.right:
		add_node( node.right, newNode )



"""def collapseDeep(node, deepMax, deepCurrent):
	if len(node["children"]) == 0:
		return node

	deepCurrent += 1
	if(deepCurrent > deepMax):
		node["children"] = getAllNodeOfHiezrachie(node)
	else:
		for i in range(0, len(node["children"])):
			collapseDeep(node["children"][i], deepMax, deepCurrent)

	return node
"""


def label_tree(node, labels, dataBrut):
	if len(node["children"]) == 0:
		leafNames = [ dataBrut[node["node_id"]][0] ]
		node["lematisation"] = labels[node["node_id"]]
	else:
		leafNames = reduce(lambda ls, c: ls + label_tree(c, labels, dataBrut), node["children"], [])

	node["name"] = "|".join(sorted(map(str, leafNames)))
	
	return leafNames


def seoCalcNodeVol_tree(node, dataBrut):
	if len(node["children"]) == 0:
		node["volume_recherche"] = dataBrut[node["node_id"]][1]
		return node["volume_recherche"]

	sumN = 0
	for i in range(0, len(node["children"])):
		sumN += seoCalcNodeVol_tree(node["children"][i], dataBrut)

	node["volume_recherche"] = sumN
	return sumN


def seoCalcNodeCPC_tree(node, dataBrut):
	if len(node["children"]) == 0:
		node["CPC"] = dataBrut[node["node_id"]][2]
		return [(node["volume_recherche"], node["CPC"])]

	interm = []
	for i in range(0, len(node["children"])):
		interm += seoCalcNodeCPC_tree(node["children"][i], dataBrut)

	sumVolume = sum([x[0] for x in interm])
	sumVolumeCPC = sum([x[0] * x[1] for x in interm])
	node["CPC"] = sumVolumeCPC / sumVolume if sumVolume != 0 else 0
	return interm


def getAllClusterLineaire(node, listeCluster):
	if len(node["children"]) != 0:
		listeCluster += [{
		"node_id" : node["node_id"],
		"CPC" : node["CPC"],
		"volume_recherche" : node["volume_recherche"],
		"enfant" : getAllNodeOfHiezrachie(node)
		}]

		for i in range(0, len(node["children"])):
			getAllClusterLineaire(node["children"][i], listeCluster)




def getAllNodeOfHiezrachie(node):
	if len(node["children"]) == 0:
		return [{
			"node_id" : node["node_id"],
			"name" : node["name"],
			"CPC" : node["CPC"],
			"volume_recherche" : node["volume_recherche"]
		}]

	listeN = []
	for i in range(0, len(node["children"])):
		listeN += getAllNodeOfHiezrachie(node["children"][i])

	return listeN


def getNodeToNDeep(node, deep = 3, currentDeep = 0, selection = []):
	if currentDeep == deep or len(node["children"]) == 0:
		selection.append(node)
	else:
		for c in node["children"]:
			getNodeToNDeep(c, deep, currentDeep + 1, selection)


def traitementSelectionNode(nodes):
	nodeSelect = []

	for i in range(0, len(nodes)):
		allSubNode = getAllNodeOfHiezrachie(nodes[i])
		allSubNode = sorted(allSubNode, key=lambda x: x["volume_recherche"], reverse=True)
		nodeSelect.append(allSubNode[0])		

	return nodeSelect