# -*- coding: utf-8 -*
import os
import unidecode
import json
import distance
import jellyfish
import numpy as np
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, to_tree, fcluster
import treeManipulation



def calcLevenshtein(words):
	return -1*np.array([[distance.levenshtein(w1,w2) for w1 in words] for w2 in words])


def calcJaro(words):
	return -1*np.array([[jellyfish.jaro_distance(w1,w2) for w1 in words] for w2 in words])


def buildClusterisation(listWord, dataBrut):
	listwordsNP = np.asarray(listWord)
	matriceDistance = calcJaro(listwordsNP)
	linked = linkage(matriceDistance, 'weighted')
	tree = to_tree( linked , rd=False )
	return (tree, linked, matriceDistance)


def buildDendrogramJson(dataBrut, options = {}):
	listWord = [x[3] for x in dataBrut]
	(tree, linked, matriceDistance) = buildClusterisation(listWord, dataBrut)
	d3Dendro = dict(children=[], name="Root1")

	treeManipulation.add_node(tree, d3Dendro)
	treeManipulation.label_tree(d3Dendro["children"][0], listWord, dataBrut)
	treeManipulation.seoCalcNodeVol_tree(d3Dendro, dataBrut)
	treeManipulation.seoCalcNodeCPC_tree(d3Dendro, dataBrut)
	
	allCluster = []
	treeManipulation.getAllClusterLineaire(d3Dendro["children"][0], allCluster)
	nodes = treeManipulation.getAllNodeOfHiezrachie(d3Dendro["children"][0])

	deepSearch = 3 if "deep" not in options else options["deep"]
	nodeInTheXDeep = []
	treeManipulation.getNodeToNDeep(d3Dendro["children"][0], deepSearch, 0, nodeInTheXDeep)
	nodeInTheXDeep = treeManipulation.traitementSelectionNode(nodeInTheXDeep)

	return {
		"dendrograme" : d3Dendro["children"][0],
		"nodeSelection" : nodeInTheXDeep,
		"clusters" : allCluster,
		"nodes" : nodes
	}


def buildDendrogramImg(dataBrut, pathFile, options = {}):
	listWord = [x[3] for x in dataBrut]
	(tree, linked, matriceDistance) = buildClusterisation(listWord, dataBrut)
	pathImg = "static/" + os.path.splitext(os.path.basename(pathFile))[0] + ".png"
	drawDendrogram(pathImg, linked, [x[0] for x in dataBrut])
	return pathImg


def drawDendrogram(path, linked, labels):
	plt.figure(figsize=(60, 41))  
	test = dendrogram(linked,  
	            orientation='top',
	            labels=labels,
	            distance_sort='descending',
	            leaf_rotation=90,
	            leaf_font_size=12,
	            show_leaf_counts=True)
	plt.savefig(path)