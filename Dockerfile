FROM alpine

LABEL maintainer "Roman Dodin <dodin.roman@gmail.com>"
LABEL description "Nginx + uWSGI + Flask based on Alpine Linux and managed by Supervisord"

RUN apk add --no-cache \
    python3 \
    bash \
    nginx \
    uwsgi \
    uwsgi-python3 \
    supervisor && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    rm /etc/nginx/conf.d/default.conf && \
    rm -r /root/.cache


RUN apk add --update curl gcc g++ python3-dev \
    && rm -rf /var/cache/apk/*

RUN ln -s /usr/include/locale.h /usr/include/xlocale.h
RUN apk --no-cache --update-cache add gfortran build-base wget freetype-dev libpng-dev openblas-dev


# Copy python requirements file
COPY ./confs/requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt


# Copy the Nginx global conf
COPY ./confs/nginx.conf /etc/nginx/
# Copy the Flask Nginx site conf
COPY ./confs/flask-site-nginx.conf /etc/nginx/conf.d/
# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY ./confs/uwsgi.ini /etc/uwsgi/
# Custom Supervisord config
COPY ./confs/supervisord.conf /etc/supervisord.conf

# Add demo app
RUN mkdir /app
WORKDIR /app

CMD ["/usr/bin/supervisord"]